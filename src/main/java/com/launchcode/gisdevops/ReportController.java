package com.launchcode.gisdevops;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(value = "/")
class BasicController {



    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public String index() {
        return "index";
    }

}
